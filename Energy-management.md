# Energy Management

## 1. Manage Energy Not Time

The image below lists the four emotional quadrants:

- Excitement
- Stress
- Depression
- Calm

[Emotional Quadrant](https://www.researchgate.net/publication/335191634/figure/fig2/AS:792212367486976@1565889555183/Modified-PA-plane-with-four-emotional-quadrants.jpg)

The idea is to remain in the Excitement, Stress and Calm quadrants for a more happy, calm and engaging life.

Stress is an inevitable part of life. We get stressed whether learning something new or doing an intense workout. But staying stressed too long can cause burnout or make us anxious and depressed.

If we welcome stress with the right mindset and then switch to activities in the Calm quadrant to relax and recover, we can get the best out of life.

We will discuss this central idea in this session.

Understanding yourself is a crucial part of the whole process.

### Question 1:

What are the activities you do that make you relax - Calm quadrant?

- Late-night drives.

- Reading books.
- Meditation.
- Trekking.
- Cooking food.

### Question 2:

When do you find getting into the Stress quadrant?

- Approaching a deadline.

- Inability to make a decision.
- Scrolling through LinkedIn.
- Family get-togethers.

### Question 3:

How do you understand if you are in the Excitement quadrant?

- I want to keep working on the activity at hand.

- I get a sense of satisfaction after completing the task.


## 2. Strategies to deal with Stress

Listed below are many strategies to beat stress. In the session, we will mostly focus on the long-term strategies that provide a solid foundation to remain in the **Excitement** and **Calm** quadrants.

### Long Term

- **Feeling the stress and completing the task**
- **Good Sleep Routine**
- **Sufficient Physical Movement**
- **Light Exercise**
- **Meditation**
- **Good structure and daily routine - having time for eating, sleeping, working and chilling**
- Yoga
- Pranayama
- Sports
- Leisure activities
- Strong relationships
- Helping Others - Acts of Kindness
- Developing Insight and Self Awareness

### Short Term

- Guided Imagery
- Progressive Muscle Relaxation (like Shavasana)
- Breathing Exercises (Boxed Breathing)
- Taking a walk
- 5, 4, 3, 2, 1 (Counting down when angry)

### Fast Acting

- Hug from a loved one
- Aromatherapy
- Taking a bath
- Massaging yourself
- Indulging in a passion or a hobby

## 3. Meditation

### Brilliant things happen in Calm minds

Video - 0:52 minutes [https://www.youtube.com/watch?v=lACf4O_eSt0](https://www.youtube.com/watch?v=lACf4O_eSt0)

### One-moment meditation

I invite you to watch the next video.

Video - 5:35 minutes

[https://www.youtube.com/watch?v=F6eFFCi12v8](https://www.youtube.com/watch?v=F6eFFCi12v8)

There is a 1-minute meditation in the video - close your eyes, play the video, relax and watch your breath and relax during that 1 minute.

If you are not convinced, you can read [benefits of meditation](https://www.healthline.com/health/mental-health/types-of-meditation).

## 4. Sleep is your superpower

Video - 19:18 minutes - [https://www.youtube.com/watch?v=5MuIMqhT8DM](https://www.youtube.com/watch?v=5MuIMqhT8DM)

### Question 4

Paraphrase the Sleep is your Superpower video in detail.

Sleep deprivation affects reproductive health in both men and women. We need sleep after and also before learning something new. Memory circuits of the brain can't retain much during the sleep deprivation phase. Lack of sleep is inversely related to memory retention power. Deep sleep brain waves act as a sort of file transfer mechanism that helps our brain map short-term memory to long-term memory.

As we grow older, our learning and memory power deteriorates. Sleep deprivation causes dementia, cognitive decline and Alzheimer's disease. 

Natural Killer cells are immune cells that identify dangerous elements in our body and help remove them. Even restricting a night's sleep to four hours can result in a drop of 70% in Natural killer cells activity. This can result in cancer cells going unchecked and ultimately growing into various types of cancer. Lack of sleep can also result in DNA cell modifications. Half of these modifications come from a decrease in immune function activity and the other half comes from tumour-promoting, inflammation, and cardiovascular disease.  


### Question 5

What are some ideas that you can implement to sleep better?


- Remove Caffeine and alcohol from your diet as they affect our sleep patterns negatively.

- Regularity in sleep patterns improves the quantity and quality of sleep.
- Decreasing room temperature helps when trying to fall asleep. 18 degrees Celcius is optimal for sleep.

## 5. Brain-Changing Benefits of Exercise

Video - 13:02 minutes - [https://www.youtube.com/watch?v=BHY0FxzoKZE](https://www.youtube.com/watch?v=BHY0FxzoKZE)

### Question 6

Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.  

- Each workout can induce mood, memory and energy boost.

- Exercise is the most transformative thing for our brain for three reasons:

    1. A single workout can immediately increase levels of neurotransmitters like dopamine, serotonin and noradrenaline.
    2. A single workout can improve the ability to shift and focus attention that lasts for many hours.
    3. Improves reaction times.

- Increase in cardiorespiratory functions changes the physiology of the brain.

- Volume of the hippocampus and pre-fontal cortex increases the more we workout.

- Neurodegenerative disease affects healthier and stronger brain lesser.

- Rule of the thumb is to get three to four times of workout sessions every week for thirty minutes each.

### Question 7

What are some steps you can take to exercise more?

- Add an extra walk around the block in your power block.

- Take stairs more often.

- Set aside some time in the day for exercise and follow that routine.
- Cycle or walk to work.
