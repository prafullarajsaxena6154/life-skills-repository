# Browser Rendering, How HTML, CSS and JavaScript files are handled.

## Browser components

The browser is a complex machine that has a lot of parts working in conjunction with one another. Let's understand about each and every one related to rendering HTML, CSS and JS files.

### 1.User interface 

 - Everything that user sees but cannot manipulate.
 - For Example: address bar, settings etc.
 - Works together with UI Backend. 

### 2.Browser engine 

 - Works as a mediating connection User interface and rendering engine.
 - When we press refresh on browser, browser engine takes the command and executes the reloading of the page.

### 3.Rendering engine

 - It parses HTML, CSS and JavaScript.
 - Based on the parsed results, it displays the page.

### 4.Networking 

 - This layer makes sure that resources are loaded based on http or https requests.

### 5.JS Interpretor 

 - Interprets the JavaScript code.

### 6.UI backend

 - Used to design basic widgets.

### 7.Data persistence

Consists of:
 - Cookies.
 - indexDB.
 - local storage.


## Basic Rendering Engine Flow

For us Rendering engine plays the most important part. This is the part which utilises the HTML, CSS and JS files, and with help of its properties and other components, displays the page as it is supposed to.

###### It works on a specific flow:

__Parsing - Render Tree - Layout/reflow - Paint(Fills the objects with colors)__

Let's start with Parsing.

## Parsing

Parsing means translation of a document into an entity that is code 
usable.

#### It is of 2 types

 - __Conventional Parser :__ For parsing __CSS and JS.__
 - __Unconventional Parser :__ For parsing __HTML.__


#### Both consists of 2 properties.

 - Vocabulary - 1,2,3,4,+,*,/ etc.
 - Syntax - num + num, num * num etc.

## How It Works

### Conventional Parsers

Uses 2 types of analysis: 

__Syntaxical Analysis:__

This analysis uses vocabulary and syntaxical grammer, makes sure that the code follows the syntax rules.

This starts with lexical analysis for the creation of tokens.

__Lexical Analysis:__

This analysis analyzes requests from Syntaxical Analysis, and uses lexers or tokenizers, which creates tokens and sends it to parser.

##### Tokens are the smallest elements that a parser can use. In Scientific terms, they are the photons if we were dealing with light!

All in all, Parser requests token, lexer sends it, either uses it based on syntax rules or stores it for later time.

__Available parsers:__

 - Flex.
 - Lex.
 - Yacc.
 - Bison.

### Unconventional Parser

HTML is not context free grammer, thats why conventional parsers cant be used.

For example, 

###### If the browser finds an error, it tries to recover from it.

If you pass an HTML file with missing closing tags, the browser might render it correctly, by automatically adding the tags.
That is why conventional parsers cannot be used. And all this is done on the basis of HTML Documention Type definition, which is pre-defined.

#### Parsing an HTML document

__Dom tree creation__ 

 - The browser looks for starting tags
 - Then tries matching it for an end tag, and creates a render tree.

__Render Tree__

 - Generated on one side while Dom tree gets constructed on the other.
 - It comprises of visual elements and which order they need to be displayed.
 - The smallest elements that the tree is made up of are called renderer or render objects. Easily identifiable as rectangles.

## Layout / Reflow

Now we have our Data parsed, how to decide what goes where? Here Layout process comes into play.

 - Layout calculates the position and size of every element recursively. 
 - Usually begins at the root of the file.

#### It is of 2 types 

##### Global Layout: 
 - Layouts the whole tree.
 - For example: Changes to font size, screen resizing.

##### Incremental layout: 
 - Layouts only the needed bits.
 - Consists of a dirty bit system.

__What is the Dirty Bit System__

 - This system makes sure that the browser doesnt do the full layout process everytime we visit the browser.
 - For eg: If new elements added deep inside the tree, then theres no need to layout the whole tree.

 _Watch this video:_

[https://www.youtube.com/watch?v=ZTnIxIA5KGw]

__Render objects are the rectangles and the layout decides where what goes.__

## Paint

We have the rectangles(render Objects), now we need to make then visually attractive and differentiable.

 - Paint goes through the whole render tree.
 - Executes recursively the paint method on each.

#### Paint is of 2 types:

##### Global Painting:
 - For painiting the whole page.

##### Incremental Painting:
 - Painiting only the crucial parts.
 - Uses the dirty bit system.

###### Painting order goes like this:

__Background color - Background Image - border - children - outline__

 ###### This is visible when one or the other file, for example image, fails. You will only see the background color.
 

## Resources

 - [https://web.dev/howbrowserswork/] How browsers work.

 - [https://limpet.net/mbrubeck/2014/08/08/toy-layout-engine-1.html] Lets build a browser engine.

  - [https://www.youtube.com/watch?v=0IsQqJ7pwhw] How browsers Work. JSUnconf.
