# **SEXUAL HARASSMENT**
Any type of behaviour, be it physical, verbal or visual, that effects working conditions and/or creates a hostilej/toxic environment, comes under Sexual Harassment.

## Types:
### Verbal Harassment

It includes comments about a person's 
- __Body__
- __Clothing__

Jokes and comments with basis on 
- __Gender__
- __Sexual provocation__ 

Requesting __Sexual__ favours

__Repeatedly inviting__ someone out, romantically

Spreading rumours about someone's __Personal life__

__Inappropriate/Sexual__ remarks, hints or suggestions

__Foul or Inappropriate__ Language or __Threats__

### Visual Harassment

It includes __Inappropriate__
- Posters
- Drawings or Sketchings
- Pictures
- Screen Savers or Wallpapers
- Emails or texts

### Physical Harassment

It includes
- __Assaulting__ someone Sexually
- Blocking or impeding someone's __movement__
- __Touching__ in anyway
- Inapproriate __Gestures, staring or uncomfortable leering__

These three 3 types come under 2 major categories, namely,

## QUID PRO QUO

Word to word, this means __This for that__.

For example, when a manager or higher authority person asks for __Inappropriate favours__ in exchange for:
- __Salary__ increase
- __Promotions__ 
- Forcing punishments or firing if __denied__

## HOSTILE WORK ENVIRONMENT

Dependent on employees. Refers to the uncomfortable/toxic environment created one employees behaviour affects another employees environment either making it intimidating or offensive.

For example,
- When repeated __sexual or inappropriate comments__ are being made, and they affect the directed employee in such a way that their work performance suffers.
- They deny __growth opportunities__, so as to avoid the harrasser.

## IMPORTANT

### In any of such situations, employee only has to __PROVE__ that the behaviour was __OFFENSIVE__ to __SOMEONE__ even if it was not the __INTENDED RECIPIENT__. 

### Company is only liable, in only 2 conditions:
- __If the employer knew, or should have known about the harassment going on.__
- __If the Company/Employer failed to take corrective measures.__

For better understanding you may refer to :

`https://www.youtube.com/watch?v=Ue3BTGW3uRQ`

# __What to do in situations like these__

1. __Document the incidents.__
2. __Report the Incidents.__
3. __Seek legal advice.__
4. __Know and assert rights.__
5. __Ensure Personal Safety.__
6. __Seek Support.__

For further clarification on how to Prove harassment, you may refer to :

`https://www.youtube.com/watch?v=m-XGNf-6Bqs`





