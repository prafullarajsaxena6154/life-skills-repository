# Question1

## What are the steps/strategies to do Active Listening?

#### Active Listening.
__It refers to the act of fully hearing and understanding what someone is saying to us.__

1. Avoid Distractions.
2. Do not interrupt the person who is speaking, Let them finish what they are saying, and then respond.
3. Try to be interested in the conversation, use door openers, for eg: Tell me more. That sounds interesting.
4. Show that you are listening, using body gestures, and hand movements etc.
5. Take notes when something important is being conversed. Paraprase the most important pieces so as to be clear about what is being understood by both parties.
6. Focus on the speaker and topic.


# Question2

## According to Fisher's model, what are the key points of Reflective Listening?

1. Listen More, talk less. 2 ears, one mouth!

2. React to what is referenced to you, dont indulge in distractions or off topics.

3. In order to restate and clarify what the other person has said, it is important to avoid asking questions or expressing the listener's feelings, beliefs, or desires. Instead, focus on paraphrasing and providing clarity to the information that was shared.

4. Trying to understand the emotions behind what the other is saying, not just the facts or ideas.

5. Taking into consideration the best drawable sense of the other's point of view. While avoiding to answer from the same.

6. Responding with acceptance and empathy.

# Question3

## What are the obstacles in your listening process?

1. Lack of focus.
2. Lack of understanding.
3. Lack of catching the exact reference, the conversation is being referred to.

# Question4

## What can you do to improve your listening?

1. Improving concentration.
2. Avoiding assumptions.
3. Asking questions wherever needed.

# Question5

## When do you switch to Passive communication style in your day to day life?

__Whenever i do silly mistakes, and am having basic questions to be clarified by peers.__

# Question6

## When do you switch into Aggressive communication styles in your day to day life?

__Whenever someone personally attacks me in a conversation, or judge my methods of doing something.__

# Question7

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

__Whenever I'm confident about a topic, and when the person infront is also talking in the same way.__

# Question8

## How can you make your communication assertive? What steps you can apply in your own life?

__In my own words and understanding, the best ways are:__

1. Ananlyse thorughly, as to what the statement means.
2. How much does it affect me.
3. If it is actually directed towards me.
4. What would be the outcome of my response, and do i want that outcome.
5. Having patience of listening more and speaking less.

