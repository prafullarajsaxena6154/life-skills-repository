## What is Deep Work?

When focusing without getting distracted, on a mentally challenging task.
Uninterrupted Focus.

## Paraphrase all the ideas in the above videos and this one in detail.

Optimal duration is around 1-1.5 hours.

It takes around 20 minutes for clearing the mind of previous thoughts, and the rest then can be invested on the actual problem. When you feel exhausted, pull back a little and then continue.
What you do in this one hour, and if it counts is valid or not.
Looking at libraries for the problem in context is valid.
Looking at mails, is not. Neither is looking at social media.

Why deadlines work? It is motivating, it helps in time blocking. A person does not have to have a debate to themself every now and then for should them take a break at that time. A deadline keeps reminding as to when the work att the time.
It does create pressure, but the urgency of getting the work done, helps a lot in completion of the work.
Keeping to deadlines help in creating discipline, which help a lot.


According to the book Deep work, deep work means professional activity performed in a state of distraction free concentration that pushes our cognitive abilities to their limits.
These push create new values, are hard to replicate and improve skills.

People like J.K. Rowling and Bill Gates alos achieved success by following deep work, but the most closest example is presented to us by the author himself, Cal Newport, who indulged in deep work and doubled his output in research papers, while maintaining a family, and teaching at a prestigious institution at the same time.

How deep work leads to it. Scientists have found out that long stretches of deep work, creates milin around the brain cells, which allows them to fire faster and cleaner. When we practice deep work, we upgrade the specific parts of our brain, to work more effectively and efficiently.
In today's world, deep work is very highly valuable, and very rare.
We are hard-wired to get distracted. In a study conducted in 2012 by Hofmann and Baumeister, temptation for distractions from work were shown to be 50% successful.

3 strategies suggested.
1. Schedule your distractions.
2. Develop a rhythmic deep work ritual.
3. Daily shutdown complete ritual.

__Consistency, and proper rest are the most important end-points from the video.__


## How can you implement the principles in your day to day life?

1. Starting the day early morning and utilising the most of it. Morning is free as we are not distracted by mails or assignments. Using that time for completion of tasks that will be fruitful with deep work.
2. Setting a time-table, when to work, when to play, when to work deeply, and when to take a rest(shutdown).
3. Staying consistent. Developing discipline, and creating the need to keep following the time-table.


## Your key takeaways from the video

3 most common objections on quitting social-media.

1. Social media is a big part of this world, and not being a part of it feels like rejecting the wonders of this fundamental technology.
2. It is vital to success in this economy. Opportunity will be diminished.
3. It's harmless, it's Fun.


Social media harms our ability to do deep work. They are designed to get addicted, they cause our attention to be fragmented. Long fragmentation, reduces our ability to do continuous deep work in a strech permanently. Loosing the ability to concentrate for stretches will be severe let-down for this economy and market.
Psychologically, Researches reveal, that the more we use social media the more we are prone to feel disconnected, and lonely. Constant exposure to a fake and only part of people's lives, leaves you feeling inadequate and doubting self-worth, leading to increased rates of depression.
Social media also causes anxiety from lack of it, because of the easy availibility of it.

Quitting social media, will be difficult for a while, but becomes a lot productive as time passes. Makes life peaceful.