#Question 1.

##Feynman Technique

__Feynman technique states that, a concept or a learning is only said to be grasped perfectly, if the person can explain it.__

For instance, we do feel sometimes, that we have understood a topic completely, but real test comes when we have to explain it someone who has no knowledge of it, for eg. a kid.

The video explains about where the saying might have come from and how it can be implemented in our learning process.

It states that the best way to follow the technique is to start by:
1. Use pen and paper.
2. Start with Writing the name of the concept on top of the page.
3. Start writing about everything you know about the topic in the simplest words possible.
4. Use diagrams and examples whenever needed.
5. Read through it again, more than often, there will always be something to add or subtract.

#Question 2.

__In my Daily learning process, I can implement the technique by__ 
1. Starting with all the 5 steps already mentioned.
2. Also doing the same steps when trying to understand the process from some content, or a peer, or from a senior.
3. Jotting down helps it to go permanent memory, and helps us for long term.

#Question 3.

__What i learnt from the video is__

The brain's functionality is complex, but it can be simplified using two methods: 
1. Focused 
2. Diffuse modes. 

In focus mode, we concentrate solely on a specific thing, eliminating other thoughts. On the other hand, diffuse mode involves a relaxed state of mind, useful when facing a problem.

Switching to diffuse mode can encourage new ideas and aid problem-solving. Learning speeds vary among individuals, with some being fast learners and others slower. Slow learners often compensate with diligent effort, leading to a higher chance of achieving a deep understanding of the subject due to the additional time spent. 

Taking breaks after learning is important, allowing us to engage in activities that bring us happiness and rejuvenation. 

#Question 4.

__Steps I can take to improve__

1. Establish a structured learning plan for specific time periods. 
2. Take regular study breaks as necessary during your learning process. 
3. Be aware that there is a possibility of forgetting previously learned information, so periodically review and recall it. 
4. Utilize note-taking to capture important details and facilitate easier retrieval during the learning process. 
5. Experiment with alternative learning approaches, such as visual aids or games, to enhance the enjoyment and effectiveness of your learning experience. 
6. Engage in discussions with peers to gain new insights and information that you might have missed. 

#Question 5.

__Key takeaways from the video__

1. When we start to learn things it gets more time, but after some practice, it gets easier and takes less time. 
2. Daily 45 minutes for 1 month will be enough. 
3. Some research says we need 10,000 hours to learn a skill which is a very long period. But in this video, we got to know if we practice 20 hours with focus. We will be very good at that skill. 

#Question 6.

__Steps I would take while approaching new topic__

1. Practice enough while learning the topic. 
2. We have to decide what topic we want to learn, and how it can be helpful for us. 
3. Find out the courses, and books which we have to follow. Set a time limit, and try to complete that topic in that fixed time. 
4. Break the topic into smaller parts, organize the path correctly, and make sure that we start from the basic. 






